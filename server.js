const express = require("express");
const bodyParser = require("body-parser");
const db = require("./db")();


const app = require("./app");
const server = require("http").Server(app);
const logger = require("./logging/logger");
const fileUpload = require('express-fileupload');

// default options
app.use(fileUpload());




// -------------------------------------------------------
// Start server
// -------------------------------------------------------

logger.info("Starting RESEARCH API server...");

db.sequelize.sync({
    force: true
}).then(() => {
    app.set("port", 3000);
    server.listen(app.get("port"), () => {
        logger.info(`'RESEARCH API listening on port'${app.get("port")}`);
    });
});