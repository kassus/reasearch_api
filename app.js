const express = require("express");
const bodyParser = require("body-parser");
const Boom = require("boom");
const path = require("path");
const cors = require("cors");


// Create Express app
const app = express();

app.use(
    bodyParser.urlencoded({
        extended: true,
        limit: "50mb"
    })
);

app.use(bodyParser.json());

app.use(cors());

const apiRoutes = require("./routes/routes");

const httpLogger = require("./logging/http-logger");


// HTTP Logger middleware
app.use(httpLogger);
// API resources routes
app.use("/api", apiRoutes);

app.get('/favicon.ico', (req, res) => res.status(204));



// API route not found handler
app.use(function (req, res, next) {
    if (!req.route)
        return next(
            Boom.notFound("Route " + req.method + " " + req.path + " not found.")
        );
    next();
});

module.exports = app;