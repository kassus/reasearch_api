module.exports = (sequelize, DataType) => {
    const Graph = sequelize.define(
        "graph", {
            id: {
                type: DataType.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
            },
            name: {
                type: DataType.TEXT,
                allowNull: false
            },
            description: {
                type: DataType.TEXT,
                allowNull: false
            },
            content: {
                type: DataType.TEXT,
                allowNull: false
            },
            startDateTime: {
                type: DataType.DATE,
                allowNull: false
            }

        }, {
            timestamps: false,
        });


    return Graph;
};