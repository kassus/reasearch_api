const express = require("express");
const router = express.Router({
    mergeParams: true
});

const graphsController = require("./graphs.controller");

router.get("/",
    graphsController.findAll,
    graphsController.responseFindAllGraphs
);

router.post("/",
    graphsController.createGraph,
    graphsController.responseCreatedGraph
);


router.put("/:id", (req, res) =>
    graphsController.updateGraph,
    graphsController.responseUpdatedGraph
);


router.delete("/:id", (req, res) =>
    graphsController.deleteGraph,
    graphsController.responseDeletedGraph
);


module.exports = router;