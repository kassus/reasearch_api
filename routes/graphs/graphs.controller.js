const db = require("../../db")();
const Graph = db.models.graph;
const logger = require("../../logging/logger");


const graphSchema = {
    type: "object",
    properties: {

        name: {
            type: "string"
        },
        description: {
            type: "string"
        },
        content: {
            type: "string"
        },

    },
    required: ["name", "description", "content"]
}


//
// CHECK FUNCTIONS
//

module.exports.checkGraphSchema = (req, res, next) => {
    const body = req.body

    console.log(body)

    if (tv4.validate(body, graphSchema)) {
        next()
    } else {
        next(boom.badData(tv4.error))
    }
};


module.exports.findAll = (req, res, next) => {
    Graph.findAll({

    }).then(
        function (graphs) {
            res.locals.graphsFromDb = graphs
            return next()
        },
        err => {
            next(err);
        }
    );

};

module.exports.createGraph = (req, res, next) => {
    Graph.create({
        name: req.body.name,
        description: req.body.description,
        content: req.body.content,

    }).then(function (graph) {
            res.locals.graphFromDb = graph
            return next()
        },
        err => {
            next(err);
        }
    );
};

module.exports.updateGraph = (req, res, next) => {
    Graph.update({
            name: req.body.name,
            description: req.body.description,
            content: req.body.content,
        }, {
            where: {
                id: req.params.id
            }
        })
        .then(function (graph) {
                res.locals.updatedGraphFromDb = graph
                return next()
            },
            err => {
                next(err);
            }
        )
};

module.exports.deleteGraph = (req, res, next) => {
    Graph.destroy({
            where: {
                id: req.params.id
            }
        })
        .then(function (graph) {
                res.locals.deletedGraphFromDb = graph
                return next()
            },
            err => {
                next(err);
            }
        )
};

module.exports.responseFindAllGraphs = (req, res, next) => {
    return res.json(res.locals.graphsFromDb);
}
module.exports.responseCreatedGraph = (req, res, next) => {
    return res.json(res.locals.graphFromDb);
}

module.exports.responseUpdatedGraph = (req, res, next) => {
    return res.json(res.locals.updatedGraphFromDb);
}

module.exports.responseDeletedGraph = (req, res, next) => {
    return res.json(res.locals.deletedGraphFromDb);
}