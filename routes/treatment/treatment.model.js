module.exports = (sequelize, DataType) => {
    const Treatment = sequelize.define(
        "treatment", {
            id: {
                type: DataType.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
            },
            name: {
                type: DataType.TEXT,
                allowNull: false
            },
            description: {
                type: DataType.TEXT,
                allowNull: false
            },
            startTime: {
                type: DataType.TEXT,
                allowNull: false
            },
            endTime: {
                type: DataType.TEXT,
                allowNull: false
            },
            spendTime: {
                type: DataType.TEXT,
                allowNull: false
            },
            spendEnergy: {
                type: DataType.TEXT,
                allowNull: false
            }



        }, {
            timestamps: false,
        });


    return Treatment;
    };