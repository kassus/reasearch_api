const db = require("../../db")();
const Treatment = db.models.treatment;
const logger = require("../../logging/logger");


const treatmentSchema = {
    type: "object",
    properties: {
        login: {
            type: "string"
        },
        name: {
            type: "string"
        },
        description: {
            type: "string"
        },
        startTime: {
            type: "string"
        },
        endTime: {
            type: "string"
        },
        spendTime: {
            type: "string"
        },
        spendEnergy: {
            type: "string"
        }
    },
    required: ["name", "description", "startTime", "endTime", "spendTime", "spendEnergy"]
}


//
// CHECK FUNCTIONS
//

module.exports.checkTreatmentSchema = (req, res, next) => {
    const body = req.body

    console.log(body)

    if (tv4.validate(body, treatmentSchema)) {
        next()
    } else {
        next(boom.badData(tv4.error))
    }
};


module.exports.findAll = (req, res, next) => {
    Treatment.findAll({

    }).then(
        function (treatment) {
            res.locals.treatmentsFromDb = treatment
            return next()
        },
        err => {
            next(err);
        }
    );

};

module.exports.createTreatment = (req, res, next) => {
    Treatment.create({
        name: req.body.name,
        description: req.body.description,
        startTime: req.body.startTime,
        endTime: req.body.endTime,
        spendTime: req.body.spendTime
    }).then(function (treatment) {
            res.locals.treatmentFromDb = treatment
            return next()
        },
        err => {
            next(err);
        }
    );
};

module.exports.updateTreatment = (req, res, next) => {
    Treatment.update({
            name: req.body.name,
            description: req.body.description,
            startTime: req.body.startTime,
            endTime: req.body.endTime,
            spendTime: req.body.spendTime
        }, {
            where: {
                id: req.params.id
            }
        })
        .then(function (treatment) {
                res.locals.updatedTreatmentFromDb = treatment
                return next()
            },
            err => {
                next(err);
            }
        )
};

module.exports.deleteTreatment = (req, res, next) => {
    Treatment.destroy({
        where: {
            id: req.params.id
        }

        .then(function (treatment) {
                res.locals.deletedTreatmentFromDb = treatment
                return next()
            },
            err => {
                next(err);
            }
        )
    });

}
module.exports.responseFindAllTreatment = (req, res, next) => {
    return res.json(res.locals.treatmentsFromDb);
}
module.exports.responseCreatedTreatment = (req, res, next) => {
    return res.json(res.locals.treatmentFromDb);
}

module.exports.responseUpdatedTreatment = (req, res, next) => {
    return res.json(res.locals.updatedTreatmentFromDb);
}

module.exports.responseDeletedTreatment = (req, res, next) => {
    return res.json(res.locals.deletedTreatmentFromDb);
}