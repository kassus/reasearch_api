const express = require("express");
const router = express.Router({
    mergeParams: true
});

const treatmentController = require("./treatment.controller");

router.get("/",
    treatmentController.findAll,
    treatmentController.responseFindAllTreatment
);

router.post("/",
    treatmentController.createTreatment,
    treatmentController.responseCreatedTreatment
);


router.put("/:id", (req, res) =>
    treatmentController.updateTreatment,
    treatmentController.responseUpdatedTreatment
);


router.delete("/:id", (req, res) =>
    treatmentController.deleteTreatment,
    treatmentController.responseDeletedTreatment
);


module.exports = router;