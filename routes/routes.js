const express = require("express");
const app = require("../app");
const router = express.Router({
    mergeParams: true
});
const logger = require("../logging/logger");

//Declare routes
router.use("/researchFile", require("./researchFiles/researchFiles.routes"));
router.use("/treatment", require("./treatment/treatment.routes"));
router.use("/graph", require("./graphs/graphs.routes"));
router.use('/user', require('./user/user.routes'));

module.exports = router;