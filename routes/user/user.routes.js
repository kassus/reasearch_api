const express = require("express");
const router = express.Router({
    mergeParams: true
});

const userController = require("./user.controller");

router.get("/",
    userController.findAll,
    userController.responseFindAllUsers
);

router.post("/",
    userController.createUser,
    userController.responseCreatedUser
);


router.put("/:id", (req, res) =>
    userController.updateUser,
    userController.responseUpdatedUser
);


router.delete("/:id", (req, res) =>
    userController.deleteUser,
    userController.responseDeletedUser
);


module.exports = router;