const db = require("../../db")();
const User = db.models.user;
const logger = require("../../logging/logger");


const userSchema = {
    type: "object",
    properties: {
        login: {
            type: "string"
        },
        password: {
            type: "string"
        },
        lastname: {
            type: "string"
        },
        firstname: {
            type: "string"
        },
        email: {
            type: "string"
        }
    },
    required: ["login", "password", "email"]
}


//
// CHECK FUNCTIONS
//

module.exports.checkUserSchema = (req, res, next) => {
    const body = req.body

    console.log(body)

    if (tv4.validate(body, userSchema)) {
        next()
    } else {
        next(boom.badData(tv4.error))
    }
};


module.exports.findAll = (req, res, next) => {
    User.findAll({

    }).then(
        function (users) {
            res.locals.usersFromDb = users
            return next()
        },
        err => {
            next(err);
        }
    );

};

module.exports.createUser = (req, res, next) => {
    User.create({
        login: req.body.login,
        password: req.body.password,
        lastname: req.body.lastname,
        firstname: req.body.firstname,
        email: req.body.email
    }).then(function (user) {
            res.locals.userFromDb = user
            return next()
        },
        err => {
            next(err);
        }
    );
};

module.exports.updateUser = (req, res, next) => {
    User.update({
            login: req.body.login,
            password: req.body.password,
            lastname: req.body.lastname,
            firstname: req.body.firstname,
            email: req.body.email
        }, {
            where: {
                id: req.params.id
            }
        })
        .then(function (user) {
                res.locals.updatedUserFromDb = user
                return next()
            },
            err => {
                next(err);
            }
        )
};

module.exports.deleteUser = (req, res, next) => {
    User.destroy({
            where: {
                id: req.params.id
            }
        }),
        err => {
            next(err);
        }
};

module.exports.responseFindAllUsers = (req, res, next) => {
    return res.json(res.locals.usersFromDb);
}
module.exports.responseCreatedUser = (req, res, next) => {
    return res.json(res.locals.userFromDb);
}

module.exports.responseUpdatedUser = (req, res, next) => {
    return res.json(res.locals.updatedUserFromDb);
}

module.exports.responseDeletedUser = (req, res, next) => {
    return res.json(res.locals.deletedUserFromDb);
}