const bcrypt = require('bcrypt');
module.exports = (sequelize, DataType) => {
    const User = sequelize.define(
        "user", {
            id: {
                type: DataType.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,

            },
            login: {
                type: DataType.STRING,
                allowNull: false,
                unique: true
            },
            password: {
                type: DataType.STRING,
                allowNull: false
            },
            lastname: {
                type: DataType.STRING,
                allowNull: false
            },
            firstname: {
                type: DataType.STRING,
                allowNull: false
            },
            email: {
                type: DataType.STRING,
                allowNull: false
            },

        }, {
            timestamps: false,
            hooks: {
                beforeCreate: user => {
                    const salt = bcrypt.genSaltSync();
                    user.password = bcrypt.hashSync(user.password, salt);
                }
            }
        });

    //Check user password
      User.isPassword = (password, encodedPassword) => {
         return bcrypt.compareSync(password, encodedPassword);
     }; 

    return User;
};