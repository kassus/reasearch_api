const express = require("express");
const router = express.Router({
    mergeParams: true
});

const researchFilesController = require("./researchFiles.controller");

router.get("/",
    researchFilesController.findAll,
    researchFilesController.responseFindAllResearchFiles
);

router.post("/",
    researchFilesController.createResearchFile,
    researchFilesController.responseCreatedResearchFile,

)


router.put("/:id", (req, res) =>
    researchFilesController.updateReasearchFile,
    researchFilesController.responseUpdatedResearchFile
);


router.delete("/:id", (req, res) =>
    researchFilesController.deleteResearchFile,
    researchFilesController.responseDeletedResearchFile
);



module.exports = router;