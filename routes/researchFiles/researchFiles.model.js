module.exports = (sequelize, DataType) => {

    const ResearchFile = sequelize.define(
        "researchFile", {
            id: {
                type: DataType.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
            },
            name: {
                type: DataType.TEXT,
                allowNull: false
            },
            description: {
                type: DataType.TEXT,
                allowNull: false
            },
            content: {
                type: DataType.TEXT,
                allowNull: false
            },
                startDateTime:{
                type: DataType.DATE,
                allowNull: false
            }

        }, {
            timestamps: false,
        });

    return ResearchFile;
};