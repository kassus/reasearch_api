const db = require("../../db")();
const express = require("express");
const ResearchFile = db.models.researchFile;
const logger = require("../../logging/logger");
const fileUpload = require('express-fileupload');
const app = express();

// default options
app.use(fileUpload());

const researchFileSchema = {
    type: "object",
    properties: {
        login: {
            type: "string"
        },
        name: {
            type: "string"
        },
        description: {
            type: "string"
        },
        content: {
            type: "string"
        }

    },
    required: ["name", "description", "content"]
}


//
// CHECK FUNCTIONS
//

module.exports.checkResearchFileSchema = (req, res, next) => {
    const body = req.body

    console.log(body)

    if (tv4.validate(body, researchFileSchema)) {
        next()
    } else {
        next(boom.badData(tv4.error))
    }
};


module.exports.findAll = (req, res, next) => {
    ResearchFile.findAll({

    }).then(
        function (researchFiles) {
            res.locals.researchFilesFromDb = researchFiles
            return next()
        },
        err => {
            next(err);
        }
    );

};

module.exports.createResearchFile = (req, res, next) => {
    ResearchFile.create({
        name: req.body.name,
        description: req.body.description,
        content: req.body.content
    }).then(function (researchFile) {
            res.locals.researchFileFromDb = researchFile
            return next()
        },
        err => {
            next(err);
        }
    );
};

module.exports.updateReasearchFile = (req, res, next) => {
    ResearchFile.update({
            name: req.body.name,
            description: req.body.description,
            content: req.body.content
        }, {
            where: {
                id: req.params.id
            }
        })
        .then(function (researchFile) {
                res.locals.updatedResearchFileFileFromDb = researchFile
                return next()
            },
            err => {
                next(err);
            }
        )
};

module.exports.deleteResearchFile = (req, res, next) => {
    ResearchFile.destroy({
        where: {
            id: req.params.id
        }

        .then(function (researchFile) {
                res.locals.deletedResearchFileFromDb = researchFile
                return next()
            },
            err => {
                next(err);
            }
        )
    });
}

module.exports.upload = (req, res, next) => {
    app.post('/upload', function (req, res) {
        if (Object.keys(req.files).length == 0) {
            return res.status(400).send('No files were uploaded.');
        }

        // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
        let sampleFile = req.files.sampleFile;

        // Use the mv() method to place the file somewhere on your server
        sampleFile.mv(req.files[0].path),
            function (err) {
                if (err)
                    return res.status(500).send(err);

                res.send('File uploaded!');
            };
    });


}

module.exports.responseFindAllResearchFiles = (req, res, next) => {
    return res.json(res.locals.researchFilesFromDb);
}
module.exports.responseCreatedResearchFile = (req, res, next) => {
    return res.json(res.locals.researchFileFromDb);
}

module.exports.responseUpdatedResearchFile = (req, res, next) => {
    return res.json(res.locals.updatedResearchchFileFromDb);
}

module.exports.responseDeletedResearchFile = (req, res, next) => {
    return res.json(res.locals.deletedResearchFileFromDb);
}